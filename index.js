const express = require('express');
const app = express();
const util = require('util');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);
const openFile = util.promisify(fs.open);

const SECRET_KEY = 'ey.pfzl3hq90b1m';
app.use(bodyParser.json({type: 'application/json'}));

app.post('/signup', async (req, res) => {
    const {username, email, password} = req.body;
    let db = JSON.parse(fs.readFileSync('./db.json', 'utf8'));

    try {
        await writeFile('./db.json', JSON.stringify([...db, {username, password, email}]));
        res.status(200).send({success: true, message: 'Changed Successfully.'}).end();

    } catch (e) {
        res.status(500).send({success: false, message: 'Server Error.'}).end();
    }
});

app.post('/login', async (req, res) => {
    const {email, password} = req.body;
    let db = JSON.parse(fs.readFileSync('./db.json', 'utf8'));


    if (db.some((user) => email === user.email && password === user.password)) {
        res.status(200).send({
            success: true,
            message: 'registered',
            token: jwt.sign({email, password}, SECRET_KEY, {expiresIn: '30min'})
        }).end();
    } else {
        res.status(401).send({success: false, message: 'Wrong Email or Password'}).end();
    }
});

app.get('/list5', async (req, res) => {


    const headers = ['header_a', 'header_b', 'header_c', 'header_d'], data = [];

    for (let i = 0; i < 50; i++) {
        let o = {};
        for (let h of headers)
            o[h] = `${h.toString()}${i}`;
        data.push(o);


    }

    res.status(200).send({
        success: true,
        data,
        headers
    }).end();

});

app.listen(3000);
